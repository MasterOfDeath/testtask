<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\MovieLike;
use Doctrine\ORM\EntityRepository;

/**
 * Class MovieLikeRepository.
 */
class MovieLikeRepository extends EntityRepository implements Contract\MovieLikeRepositoryInterface
{
    /**
     * @param MovieLike $like
     */
    public function save(MovieLike $like)
    {
        $this->getEntityManager()->persist($like);
        $this->getEntityManager()->flush();
    }
}
