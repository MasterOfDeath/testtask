<?php
/**
 * 2019-06-28.
 */

declare(strict_types=1);

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use App\Entity\Movie;

/**
 * Class MovieRepository.
 */
class MovieRepository extends EntityRepository implements Contract\MovieRepositoryInterface
{
    /**
     * @param int $movieId
     *
     * @throws Exception\NotFoundException
     *
     * @return Movie
     */
    public function get(int $movieId): Movie
    {
        $movie = $this->find($movieId);
        if (empty($movie)) {
            throw new Exception\NotFoundException('Movie not found');
        }

        return $movie;
    }
}
