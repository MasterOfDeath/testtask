<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * Class UserRepository.
 */
class UserRepository extends EntityRepository implements Contract\UserRepositoryInterface
{
    /**
     * @param int $userId
     *
     * @throws Exception\NotFoundException
     *
     * @return User
     */
    public function get(int $userId): User
    {
        $user = $this->find($userId);
        if (empty($user)) {
            throw new Exception\NotFoundException('User not found');
        }

        return $user;
    }

    /**
     * @param string $username
     *
     * @return User|null
     */
    public function getByUsername(string $username): ?User
    {
        $users = $this->findBy(['username' => $username], null, 1);

        return $users[0] ?: null;
    }

    /**
     * @param User $user
     */
    public function save(User $user)
    {
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }
}
