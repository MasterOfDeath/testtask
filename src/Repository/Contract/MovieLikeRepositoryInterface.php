<?php

declare(strict_types=1);

namespace App\Repository\Contract;

use App\Entity\MovieLike;

interface MovieLikeRepositoryInterface
{
    public function save(MovieLike $like);
}
