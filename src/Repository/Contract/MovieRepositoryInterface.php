<?php

declare(strict_types=1);

namespace App\Repository\Contract;

use App\Entity\Movie;

interface MovieRepositoryInterface
{
    public function get(int $movieId): Movie;
}
