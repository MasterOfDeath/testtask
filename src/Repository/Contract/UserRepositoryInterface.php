<?php

declare(strict_types=1);

namespace App\Repository\Contract;

use App\Entity\User;

interface UserRepositoryInterface
{
    public function get(int $userId): User;

    public function getByUsername(string $username): ?User;

    public function save(User $user);
}
