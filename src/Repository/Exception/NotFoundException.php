<?php

namespace App\Repository\Exception;

class NotFoundException extends \LogicException
{
}
