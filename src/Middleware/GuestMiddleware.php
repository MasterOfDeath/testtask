<?php

namespace App\Middleware;

use Slim\Psr7\Response;
use Slim\Interfaces\RouteCollectorInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use App\Service\Contract\UserServiceInterface;

class GuestMiddleware extends Middleware
{
    /**
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $user = $this->container->get(UserServiceInterface::class)->user();
        if ($user) {
            $response = new Response();
            $routeParser = $this->container
                ->get(RouteCollectorInterface::class)
                ->getRouteParser()
            ;

            return $response
                ->withHeader('Location', $routeParser->urlFor('main'))
                ->withStatus(302)
            ;
        }

        return $handler->handle($request);
    }
}
