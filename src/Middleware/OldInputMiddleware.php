<?php

namespace App\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Views\Twig;

class OldInputMiddleware extends Middleware
{
    /**
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $twig = $this->container->get(Twig::class);
        $twig->getEnvironment()->addGlobal('oldpost', $_SESSION['oldpost']);
        $_SESSION['oldpost'] = $request->getParsedBody();

        return $handler->handle($request);
    }
}
