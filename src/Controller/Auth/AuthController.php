<?php

declare(strict_types=1);

namespace App\Controller\Auth;

use App\Service\Contract\UserServiceInterface;
use App\Service\Exception\BusinessRuleException;
use App\Service\Dto\UserCreate;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Interfaces\RouteCollectorInterface;
use Twig\Environment;
use Slim\Flash;
use Psr\Container\ContainerInterface;

/**
 * Class MovieController.
 */
class AuthController
{
    /**
     * @var RouteCollectorInterface
     */
    private $routeCollector;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var UserServiceInterface
     */
    private $userService;

    /**
     * @var Flash\Messages
     */
    private $flash;

    /**
     * MovieController constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->routeCollector = $container->get(RouteCollectorInterface::class);
        $this->twig = $container->get(Environment::class);
        $this->userService = $container->get(UserServiceInterface::class);
        $this->flash = $container->get(Flash\Messages::class);
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     *
     * @return ResponseInterface
     */
    public function getSignUp(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $data = $this->twig->render('auth/signup.html.twig');
        $response->getBody()->write($data);

        return $response;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     *
     * @return ResponseInterface
     */
    public function postSignUp(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $params = (array) $request->getParsedBody();

        $userForm = new UserCreate();
        $userForm->name = htmlspecialchars($params['name']);
        $userForm->username = htmlspecialchars($params['username']);
        $userForm->password = $params['password'];
        $userForm->confirm = $params['confirm'];

        $routeParser = $this->routeCollector->getRouteParser();

        try {
            $this->userService->signUp($userForm);
        } catch (BusinessRuleException $ex) {
            $this->flash->addMessage('signup-error', $ex->getMessage());

            return $response
                ->withHeader('Location', $routeParser->urlFor('auth.signup'))
                ->withStatus(302)
            ;
        }

        $this->userService->signIn($userForm->username, $userForm->password);

        $this->flash->addMessage('main-info', 'You have been signed up!');

        return $response
            ->withHeader('Location', $routeParser->urlFor('main'))
            ->withStatus(302)
        ;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     *
     * @return ResponseInterface
     */
    public function getSignIn(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $data = $this->twig->render('auth/signin.html.twig');
        $response->getBody()->write($data);

        return $response;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     *
     * @return ResponseInterface
     */
    public function postSignOut(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $this->userService->signOut();

        $routeParser = $this->routeCollector->getRouteParser();

        return $response
            ->withHeader('Location', $routeParser->urlFor('main'))
            ->withStatus(302)
        ;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     *
     * @return ResponseInterface
     */
    public function postSignIn(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $params = (array) $request->getParsedBody();
        $username = htmlspecialchars($params['username']);
        $password = (string) $params['password'];

        $routeParser = $this->routeCollector->getRouteParser();

        try {
            $this->userService->signIn($username, $password);
        } catch (BusinessRuleException $ex) {
            $this->flash->addMessage('signin-error', $ex->getMessage());

            return $response
                ->withHeader('Location', $routeParser->urlFor('auth.signin'))
                ->withStatus(302)
            ;
        }

        return $response
            ->withHeader('Location', $routeParser->urlFor('main'))
            ->withStatus(302)
        ;
    }
}
