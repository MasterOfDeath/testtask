<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Movie;
use App\Service\Contract\MovieServiceInterface;
use App\Service\Exception\ValidateException;
use App\Repository\Exception\NotFoundException as RepositoryNotFoundException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Container\ContainerInterface;
use Slim\Exception\HttpNotFoundException;
use Slim\Interfaces\RouteCollectorInterface;
use Slim\Flash;
use Twig\Environment;

/**
 * Class MovieController.
 */
class MovieController
{
    /** @var RouteCollectorInterface */
    private $routeCollector;

    /** @var Environment */
    private $twig;

    /** @var \App\Repository\MovieRepository */
    private $movies;

    /** @var MovieServiceInterface */
    private $movieService;

    /**
     * @var Flash\Messages
     */
    private $flash;

    /**
     * MovieController constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $em = $container->get(EntityManagerInterface::class);

        $this->routeCollector = $container->get(RouteCollectorInterface::class);
        $this->twig = $container->get(Environment::class);
        $this->movies = $em->getRepository(Movie::class);
        $this->movieService = $container->get(MovieServiceInterface::class);
        $this->flash = $container->get(Flash\Messages::class);
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     * @param array                  $args
     *
     * @throws HttpNotFoundException
     *
     * @return ResponseInterface
     */
    public function get(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        try {
            $movieId = (int) $args['id'];
            $movie = $this->movies->get($movieId);
        } catch (RepositoryNotFoundException $ex) {
            throw new HttpNotFoundException($request, $ex->getMessage(), $ex);
        }

        $data = $this->twig->render('movies/detail.html.twig', compact('movie'));

        $response->getBody()->write($data);

        return $response;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     *
     * @return ResponseInterface
     */
    public function addLike(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $params = (array) $request->getParsedBody();
        $movieId = (int) $params['movieId'];

        $routeParser = $this->routeCollector->getRouteParser();

        try {
            $this->movieService->addLike($movieId);
        } catch (ValidateException $ex) {
            $this->flash->addMessage('main-error', $ex->getMessage());
        }

        return $response
            ->withHeader('Location', $routeParser->urlFor('movie.detail', ['id' => $movieId]))
            ->withStatus(302)
        ;
    }
}
