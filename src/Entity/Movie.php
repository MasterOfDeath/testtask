<?php
/**
 * 2019-06-28.
 */

declare(strict_types=1);

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MovieRepository")
 * @ORM\Table(name="movie", indexes={@Index(columns={"title"})})
 */
class Movie
{
    /**
     * @var int|null
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     * @ORM\Column()
     */
    private $title;

    /**
     * @var string|null
     * @ORM\Column()
     */
    private $link;

    /**
     * @var string|null
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var DateTimeImmutable|null
     * @ORM\Column(type="datetime", name="pub_date")
     */
    private $pubDate;

    /**
     * @var string|null
     * @ORM\Column(nullable=true)
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MovieLike", mappedBy="movie")
     */
    private $likes;

    public function __construct()
    {
        $this->likes = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string|null $image
     *
     * @return Movie
     */
    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     *
     * @return Movie
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @param string|null $link
     *
     * @return Movie
     */
    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return Movie
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getPubDate(): ?DateTimeImmutable
    {
        return $this->pubDate;
    }

    /**
     * @param DateTimeImmutable|null $pubDate
     *
     * @return Movie
     */
    public function setPubDate(?DateTimeImmutable $pubDate): self
    {
        $this->pubDate = $pubDate;

        return $this;
    }

    /**
     * @return Collection|MovieLike[]
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @return MovieLike|null
     */
    public function getMyOwnLike(?User $user = null)
    {
        if ($user) {
            $criteria = Criteria::create()
                ->where(Criteria::expr()->eq('user', $user))
                ->setFirstResult(0)
                ->setMaxResults(1)
            ;

            $myLike = $this->likes->matching($criteria);

            return $myLike->first() ?: null;
        }

        return null;
    }
}
