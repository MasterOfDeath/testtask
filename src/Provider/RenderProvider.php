<?php

declare(strict_types=1);

namespace App\Provider;

use App\Support\Config;
use App\Support\ServiceProviderInterface;
use App\Service\Contract\UserServiceInterface;
use Psr\Container\ContainerInterface;
use Slim\Interfaces\RouteCollectorInterface;
use UltraLite\Container\Container;
use Slim\Views\TwigMiddleware;
use Slim\Views\Twig;
use Twig\Environment;
use Odan\Csrf\CsrfMiddleware;

/**
 * Class RenderProvider.
 */
class RenderProvider implements ServiceProviderInterface
{
    /**
     * @param Container $container
     *
     * @return mixed|void
     */
    public function register(Container $container)
    {
        $container->set(Twig::class, static function (ContainerInterface $container) {
            $config = $container->get(Config::class);
            $cache = $config->get('templates')['cache'];
            $options = [
                'cache' => empty($cache) || $config->get('environment') === 'dev' ? false : $cache,
            ];

            return new Twig($config->get('templates')['dir'], $options);
        });

        $container->set(Environment::class, static function (ContainerInterface $container) {
            $twig = $container->get(Twig::class)->getEnvironment();

            // Twig Flash Extension
            $twig->addExtension(new \Knlv\Slim\Views\TwigMessages(
                new \Slim\Flash\Messages()
            ));

            // Add CSRF token as global template variable
            $csrfToken = $container->get(CsrfMiddleware::class)->getToken();
            $twig->addGlobal('csrf_token', $csrfToken);

            // Insert user object
            $user = $container->get(UserServiceInterface::class)->user();
            $twig->addGlobal('user', $user);

            return $twig;
        });

        $container->set(TwigMiddleware::class, static function (ContainerInterface $container) {
            $twig = $container->get(Twig::class);
            $routeParser = $container->get(RouteCollectorInterface::class)->getRouteParser();

            $twigMiddleware = new TwigMiddleware($twig, $routeParser);

            return $twigMiddleware;
        });
    }
}
