<?php

namespace App\Service;

use App\Entity\User;
use Assert\Assert;
use Assert\Assertion;
use Assert\InvalidArgumentException;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\Exception\BusinessRuleException;
use App\Repository\Exception\NotFoundException;

class UserService implements Contract\UserServiceInterface
{
    /** @var int */
    private $minNameLength = 3;

    /** @var int */
    private $minUsernameLength = 5;

    /** @var int */
    private $minPasswordLength = 6;

    /**
     * @var \App\Repository\UserRepository
     */
    private $users;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->users = $em->getRepository(User::class);
    }

    /**
     * @param Dto\UserCreate $userForm
     *
     * @throws BusinessRuleException
     */
    public function signUp(Dto\UserCreate $userForm)
    {
        $this->validateSignUp($userForm);

        $passwordhash = (string) password_hash($userForm->password, PASSWORD_DEFAULT);

        $user = (new User())
            ->setName($userForm->name)
            ->setUsername($userForm->username)
            ->setPasswordhash($passwordhash)
        ;

        $this->users->save($user);
    }

    /**
     * @param string $username
     * @param string $password
     *
     * @throws BusinessRuleException
     */
    public function signIn(string $username, string $password)
    {
        try {
            Assert::lazy()
                ->that($username)->notBlank('Username is required')
                ->that($password)->notBlank('Password is required')
                ->verifyNow()
            ;

            $user = $this->users->getByUsername($username);
            Assertion::notNull($user, 'Incorrect username or password');

            $verifyResult = password_verify($password, $user->getPasswordhash());
            Assertion::true($verifyResult, 'Incorrect username or password');
        } catch (InvalidArgumentException $ex) {
            throw new BusinessRuleException($ex->getMessage(), (int) $ex->getCode(), $ex->getPrevious());
        }

        $_SESSION['user'] = $user->getId();
    }

    public function signOut()
    {
        unset($_SESSION['user']);
    }

    /**
     * Get authorized user.
     *
     * @return User|null
     */
    public function user(): ?User
    {
        $userId = (int) $_SESSION['user'];
        if (!empty($userId)) {
            try {
                return $this->users->get($userId);
            } catch (NotFoundException $ex) {
                return null;
            }
        }

        return null;
    }

    /**
     * Validate data before sign up.
     *
     * @param Dto\UserCreate $userForm
     *
     * @throws BusinessRuleException
     */
    private function validateSignUp(Dto\UserCreate $userForm)
    {
        try {
            Assert::lazy()
                ->that($userForm->name)->tryAll()
                    ->notBlank('Name is required')
                    ->minLength($this->minNameLength, 'Min length of Name is: ' . $this->minNameLength)
                ->that($userForm->username)->tryAll()
                    ->notBlank('Username is required')
                    ->notContains(' ', 'Username must not contain spaces')
                    ->minLength($this->minUsernameLength, 'Min length of Username is: ' . $this->minUsernameLength)
                ->that($userForm->password)->tryAll()
                    ->notBlank('Password is required')
                    ->notContains(' ', 'Password must not contain spaces')
                    ->minLength($this->minPasswordLength, 'Min length of Password is: ' . $this->minPasswordLength)
                ->that($userForm->confirm)->tryAll()
                    ->notBlank('Password Confirm is required')
                    ->same($userForm->password, 'Confirm and Password have to be same')
                ->verifyNow()
            ;

            Assertion::null(
                $this->users->getByUsername($userForm->username),
                'User with username: ' . $userForm->username . ' already exist'
            );
        } catch (InvalidArgumentException $ex) {
            throw new BusinessRuleException($ex->getMessage(), (int) $ex->getCode(), $ex->getPrevious());
        }
    }
}
