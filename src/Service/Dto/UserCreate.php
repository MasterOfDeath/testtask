<?php

namespace App\Service\Dto;

class UserCreate
{
    /** @var string */
    public $name;

    /** @var string */
    public $username;

    /** @var string */
    public $password;

    /** @var string */
    public $confirm;
}
