<?php

namespace App\Service;

use App\Entity\Movie;
use App\Entity\MovieLike;
use App\Service\Contract\UserServiceInterface;
use App\Repository\MovieLikeRepository;
use App\Repository\MovieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use App\Service\Exception\BusinessRuleException;

class MovieService implements Contract\MovieServiceInterface
{
    /**
     * @var MovieLikeRepository
     */
    private $likes;

    /**
     * @var MovieRepository
     */
    private $movies;

    /**
     * @var UserServiceInterface
     */
    private $userService;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $em = $container->get(EntityManagerInterface::class);

        $this->likes = $em->getRepository(MovieLike::class);
        $this->movies = $em->getRepository(Movie::class);
        $this->userService = $container->get(UserServiceInterface::class);
    }

    /**
     * @param int $movieId
     *
     * @throws BusinessRuleException
     * @throws \App\Repository\Exception\NotFoundException
     */
    public function addLike(int $movieId)
    {
        $movie = $this->movies->get($movieId);
        $user = $this->userService->user();
        if (!$user) {
            throw new BusinessRuleException('Only authorized user can like a movie');
        }

        $like = (new MovieLike())
            ->setUser($user)
            ->setMovie($movie)
        ;

        $this->likes->save($like);
    }
}
