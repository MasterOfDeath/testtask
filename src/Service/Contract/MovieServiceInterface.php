<?php

namespace App\Service\Contract;

interface MovieServiceInterface
{
    public function addLike(int $movieId);
}
