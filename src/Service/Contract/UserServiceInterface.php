<?php

namespace App\Service\Contract;

use App\Service\Dto\UserCreate;
use App\Entity\User;

interface UserServiceInterface
{
    public function signUp(UserCreate $userForm);

    public function signIn(string $username, string $password);

    public function signOut();

    public function user(): ?User;
}
