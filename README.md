Тестовое задание для PHP-программиста
=====================================

Локальное развертывание
--------------

- В корне приложения добавить файл docker-compose.override.yaml с содержанием:
```
version: '3.7'

services:
  nginx:
    image: nginx:latest
    ports:
        - "8088:80"
        - "4443:443"
    volumes:
        - ./nginx/conf.d:/etc/nginx/conf.d
        - ./nginx/logs:/var/log/nginx/
        - .:/var/www/app
    links:
        - app
```
- В корне приложения добавить файл .env.local с содержанием:
```
APP_ENV=prod
DATABASE=mysql://webmaster:webmaster@database:3306/slim_project
```
- Запуск приложения:

`docker-compose up -d`

- Установка зависимостей

`docker-compose exec app composer install`

- Создание БД:

`docker-compose exec app bin/console orm:schema-tool:create`

- Импорт данных:

`docker-compose exec app bin/console fetch:trailers`

- Само приложение доступно по адресу:

`http://localhost:8088`

Могут быть проблемы с правами доступа в проекте, в локальном окружение (не production)
можно просто предоставить полный доступ к папке var и nginx/logs:
`chmod -R 777 var nginx/logs`
